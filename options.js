// Site Pause allows you to take conciouse pause from site you don't want to visit 
// Copyright (C) 2019 Lars Nielsen

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function saveOptions(e) {
    console.log("am here"); 
    browser.storage.sync.set({
        site_pause_redirect_site: document.querySelector("#new-redirect-site").value
    });
    e.preventDefault();
}

function restoreOptions() {
  var storageItem = browser.storage.managed.get('site_pause_redirect_site');
  storageItem.then((res) => {
    document.querySelector("#current-redirect-site").innerText = res.colour;
  });

  var gettingItem = browser.storage.sync.get('site_pause_redirect_site');
  gettingItem.then((res) => {
    document.querySelector("#current-redirect-site").value = res.site_pause_redirect_site;
  });
}

document.addEventListener('DOMContentLoaded', restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);

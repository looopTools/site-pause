# Site Pause 1.0.0

Site Pause is a Firefox extension which allows you to block your own
access to websites. 

# Road map

## 1.0.0

- Support blocking of multiple websites
- Support adding and removal of websites 

## 2.0.0

- Add support for time span for block 
- Add Score board for fails 
- Add track how long since last page visit

# People 

**Maintainer:** Lars Nielsen (looopTools)


## Special thanks - Contributors 
